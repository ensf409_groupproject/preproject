
/**
 * This class is made up of nested classes that contain the ActionListeners for
 * every object in the FrontEnd
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 */
import java.awt.event.*;
import java.io.*;
import java.util.Scanner;
import javax.swing.*;

public class MainViewController {

	/**
	 * The main frame for the Student record application
	 */
	MainViewDisplay theMainViewDisplay;
	/**
	 * the insert frame for the insert record window
	 */
	InsertNodeFrame theInsertNodeFrame;

	/**
	 * constructs and object of type MainViewController and calls the method
	 * initController() to connect the buttons of theMainViewDisplay to the action
	 * listener
	 *
	 * @param theFrontEnd
	 * @param theInsertNodeFrame
	 */
	public MainViewController(MainViewDisplay theFrontEnd, InsertNodeFrame theInsertNodeFrame) {
		this.theMainViewDisplay = theFrontEnd;
		this.theInsertNodeFrame = theInsertNodeFrame;
		initController();
	}

	/**
	 *
	 * makes theInsertNodeFrame visible if the InsertListener action is performed
	 *
	 */
	public class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			theInsertNodeFrame.setVisible(true);
		}
	}

	/**
	 * Listens for the find button to be clicked and then finds a match for the
	 * specified Student id number
	 */
	public class FindListener implements ActionListener {

		/**
		 * the id of the student
		 */
		String id = null;
		/**
		 * binary search tree of student records
		 */
		BinSearchTree myTree;

		/**
		 * constructs an object of type FindListener
		 *
		 * @param myTree the BinSearchTree full of records
		 */
		public FindListener(BinSearchTree myTree) {
			this.myTree = myTree;
		}

		/**
		 * prompts the user for input if the ActionEvent e occurs, searches the tree for
		 * a matching record and displays the result
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				id = JOptionPane.showInputDialog("Please enter the student's id").trim();
				Node match = theMainViewDisplay.getBinSearchTree().find(myTree.root, id);
				if (match == null) {
					JOptionPane.showMessageDialog(null, "No match for student id");

				} else {
					JOptionPane.showMessageDialog(null, match);
				}
			} catch (Exception e1) {
			}

		}
	}

	/**
	 * Listens for the browse button to be pressed and then prints out the student
	 * records to the JTextArea
	 *
	 *
	 *
	 */
	public class BrowseListener implements ActionListener {

		/**
		 * the text area where the binary search tree will be printed
		 */
		JTextArea textArea;
		/**
		 * binary search tree of student records
		 */
		BinSearchTree myTree;

		/**
		 * constructs an object of type BrowseListener
		 *
		 * @param textArea
		 * @param myTree
		 */
		public BrowseListener(JTextArea textArea, BinSearchTree myTree) {

			this.textArea = textArea;
			this.myTree = myTree;
		}

		/**
		 * listens for the Browse button to be pressed and then prints the tree to the
		 * JTextArea
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {

				this.textArea.setText("");
				myTree.print_tree(myTree.root, textArea);
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (NullPointerException npe) {
				JOptionPane.showMessageDialog(null, "Open the file, then browse.");

			}

		}
	}

	/**
	 * Listens for the CreateTreeFromFile button to be pressed and then prompts the
	 * user for a filename and reads the contents into a binary search tree
	 *
	 *
	 */
	public class CreateListener implements ActionListener {

		/**
		 * user inputed filename containing student records
		 */
		String filename;
		/**
		 * binary search tree that will be created from the records
		 */
		BinSearchTree myTree;

		/**
		 * constructs an object of type createListener
		 *
		 * @param myTree
		 */
		public CreateListener(BinSearchTree myTree) {
			this.myTree = myTree;
		}

		/**
		 * listens for the create tree button to be clicked and then creates a popup
		 * pane that requests user input, calls the createtree function
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				filename = JOptionPane.showInputDialog("Enter the file name:");
				createTree();
			} catch (NullPointerException ee) {
			}
		}

		/**
		 * opens an scanner object using the filename and reads Student records and
		 * insert them into the tree
		 */
		private void createTree() {

			try {
				Scanner input = new Scanner(new File(filename));
				String id, faculty, major, year;
				while (input.hasNext()) {
					id = input.next();
					faculty = input.next();
					major = input.next();
					year = input.next();
					myTree.insert(id, faculty, major, year);

				}
				input.close();
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, "Error opening file.");

			}
		}
	}

	/**
	 * Initializes the controller for the GUI. That is, it "wires" up all buttons to
	 * their respective actions.
	 */
	private void initController() {
		this.theMainViewDisplay.getInsert().addActionListener(new InsertListener());
		this.theMainViewDisplay.getFind().addActionListener(new FindListener(theMainViewDisplay.getBinSearchTree()));
		this.theMainViewDisplay.getBrowse().addActionListener(
				new BrowseListener(theMainViewDisplay.getRecords(), theMainViewDisplay.getBinSearchTree()));
		this.theMainViewDisplay.getCreateTree()
				.addActionListener(new CreateListener(theMainViewDisplay.getBinSearchTree()));

	}

}
