
import java.awt.BorderLayout;
import java.awt.Container;

import javax.swing.*;

/**
 * Contains data fields and methods to create and display the main window of the
 * student record application
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class MainViewDisplay extends JFrame {
	/**
	 * buttons for the main display that correspond to the purposes insert record,
	 * find record, browse(display records), and create a binary search tree from a
	 * file
	 */
	private JButton insert, find, browse, createTree;
	/**
	 * the area where the records will be displayed
	 */
	private JTextArea records;
	/**
	 * the BinSearchTree that contains the student records
	 */
	private BinSearchTree myTree;

	/**
	 * constructs an object of type MainViewDisplay
	 * 
	 * @param s      the name of the window displayed at the top
	 * @param myTree the binSearchTree that contains the student records
	 */
	public MainViewDisplay(String s, BinSearchTree myTree) {
		super(s);
		Container c = this.getContentPane();

		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setResizable(false);
		this.myTree = myTree;

		records = new JTextArea(25, 30);
		insert = new JButton("Insert");
		find = new JButton("Find");
		browse = new JButton("Browse");
		createTree = new JButton("Create Tree from File");
		JLabel title = new JLabel("An Application to Maintain Student Records", SwingConstants.CENTER);
		c.add(title, "North");
		JPanel myPanel = new JPanel();
		myPanel.add(insert);
		myPanel.add(find);
		myPanel.add(browse);
		myPanel.add(createTree);
		c.add(myPanel, "South");
		JPanel myText = new JPanel();
		JScrollPane scrollPane = new JScrollPane(records);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setVerticalScrollBar(new JScrollBar());
		records.setEditable(false);
		myText.add(scrollPane);
		c.add(myText, "Center");
		this.setSize(500, 500);
		this.setVisible(true);
	}

	/**
	 * Returns the BinSearchTree containing student records
	 * 
	 * @return the BinSearchTree myTree
	 */
	public BinSearchTree getBinSearchTree() {
		return this.myTree;
	}

	/**
	 * Returns the "Insert" button for the frame
	 * 
	 * @return insert
	 */
	public JButton getInsert() {
		return insert;
	}

	/**
	 * Returns the "find" button for the frame
	 * 
	 * @return find
	 */
	public JButton getFind() {
		return find;
	}

	/**
	 * returns the "browse" button for the frame
	 * 
	 * @return browse
	 */
	public JButton getBrowse() {
		return browse;
	}

	/**
	 * Returns the "createTree" button for the frame
	 * 
	 * @return
	 */
	public JButton getCreateTree() {
		return createTree;
	}

	/**
	 * Returns the JTextArea "records for the frame
	 * 
	 * @return records
	 */
	public JTextArea getRecords() {
		return records;
	}

}
