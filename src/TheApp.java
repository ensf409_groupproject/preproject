/**
 * contains the main function that runs the student records application
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 *
 */
public class TheApp {

	public static void main(String[] args) {
		BinSearchTree studentRecords = new BinSearchTree();
		MainViewDisplay myMainFrame = new MainViewDisplay("Main Frame", studentRecords);
		InsertNodeFrame insertBtn = new InsertNodeFrame(studentRecords);

		MainViewController theMainViewsController = new MainViewController(myMainFrame, insertBtn);
		InsertFrameController theInsertFrameController = new InsertFrameController(insertBtn);
	}
}
