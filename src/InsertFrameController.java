
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * This class is made up of nested classes that contain the ActionListeners for
 * every object in the FrontEnd
 * 
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 */
public class InsertFrameController {
	/**
	 * the InsertNodeFrame for the insert function of the app and calls the method
	 * initController() to connect the buttons of InsertNodeFrame to the action
	 * listener
	 */
	InsertNodeFrame theInsertNodeFrame;

	/**
	 * constructs an object of type InsertFrameController and
	 * 
	 * @param theInsertNodeFrame
	 */
	public InsertFrameController(InsertNodeFrame theInsertNodeFrame) {
		this.theInsertNodeFrame = theInsertNodeFrame;
		initComponents();
	}

	/**
	 * Listens for the insert button to be clicked and then makes theInsertNodeFrame
	 * visible
	 */
	public class InsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			theInsertNodeFrame.setVisible(true);
		}
	}

	/**
	 * Inserts a new node into the BinSearchTree based on the user input
	 */
	public class InsertFrameInsertListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			theInsertNodeFrame.insertNode();
		}
	}

	/**
	 * Returns to the Main Window from the InsertNodeFrame when clicking the "return
	 * to main window" button
	 */
	public class InsertFrameReturnToMainWindow implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent ae) {
			theInsertNodeFrame.returnToMainWindow();
		}

	}

	/**
	 * Initializes the controller for the theInsertNodeFram. That is, it "wires" up
	 * all buttons to their respective actions.
	 */
	private void initComponents() {
		this.theInsertNodeFrame.getInsertBtn().addActionListener(new InsertFrameInsertListener());
		this.theInsertNodeFrame.getReturnBtn().addActionListener(new InsertFrameReturnToMainWindow());
	}
}
