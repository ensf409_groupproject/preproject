
import java.awt.Container;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 * This is the window that appears when the user tries to Insert a New Node
 *
 * @author Nicholas Cervania
 * @author Jannalie Taylor
 * @author Negar Tajziyehchi
 */
public class InsertNodeFrame extends JFrame {

    /**
     * the fields where the user will input studentID, faculty, major and year
     */
    private JTextField studentID, faculty, major, year;
    /**
     * inserts the student, returns to main window without inserting a student
     */
    private JButton insertFrameInsertBtn, insertFrameReturnToMainWindowBtn;
    /**
     * the BinarySearchTree containing student records
     */
    private BinSearchTree theTree;

    /**
     * Constructs a new InsertNodeFrame
     *
     * @param theTree the BinSearchTree object
     */
    public InsertNodeFrame(BinSearchTree theTree) {
        super();

        this.theTree = theTree;

        initializeInsertFrame();

        this.setVisible(false);
        this.setSize(500, 200);
        this.setResizable(false);
    }

    /**
     * Initializes all components of the frame and places them on the pane
     *
     */
    private void initializeInsertFrame() {

        studentID = new JTextField("", 5);
        faculty = new JTextField("", 10);
        major = new JTextField("", 20);
        year = new JTextField("", 5);

        insertFrameInsertBtn = new JButton("Insert");
        insertFrameReturnToMainWindowBtn = new JButton("Return to Main Window");
        JPanel thePanel = new JPanel();

        Container container = this.getContentPane();
        thePanel.add(new JLabel("Insert a New Node"));
        container.add(thePanel, "North");

        thePanel = new JPanel();
        thePanel.add(new JLabel("Enter The Student ID"));
        thePanel.add(studentID);
        thePanel.add(new JLabel("Enter Faculty"));
        thePanel.add(faculty);
        thePanel.add(new JLabel("Enter Student's Major"));
        thePanel.add(major);
        thePanel.add(new JLabel("Enter Year"));
        thePanel.add(year);
        container.add(thePanel, "Center");

        thePanel = new JPanel();
        thePanel.add(insertFrameInsertBtn);
        thePanel.add(insertFrameReturnToMainWindowBtn);
        container.add(thePanel, "South");
    }

    /**
     * Clears all text boxes and sets this frame's visibility to false
     */
    public void returnToMainWindow() {
        this.setVisible(false);
        studentID.setText("");
        faculty.setText("");
        major.setText("");
        year.setText("");
    }

    /**
     * inserts a new node in the BinSearchTree based on the user input
     */
    public void insertNode() {
        if (!(studentID.getText().isEmpty() || faculty.getText().isEmpty() || major.getText().isEmpty() || year.getText().isEmpty())) {
            theTree.insert(studentID.getText(), faculty.getText(), major.getText(), year.getText());
        }

        returnToMainWindow();
    }

    /**
     * Returns the Insert button for the frame
     *
     * @return the Insert button
     */
    public JButton getInsertBtn() {
        return insertFrameInsertBtn;
    }

    /**
     * Returns the "Return to Main Window" button for the frame
     *
     * @return the "Return to Main Window" button for the frame
     */
    public JButton getReturnBtn() {
        return insertFrameReturnToMainWindowBtn;
    }

}
